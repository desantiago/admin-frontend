This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3002](http://localhost:3002) with your browser to see the result.

Admin application for the store.

Where the users can update and add new products and all the data that a store needs.

It's a NextJS/React application

Main Dependencies: 
- NextJS and React
- Apollo GraphQL Client

Will update the config options later.

It's Work in progress is going to change a lot in the following commits.

