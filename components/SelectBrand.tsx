export default function SelectBrand({ brand, brands, onChange }) {

    const handleChange = (e) => {
        let { value } = e.target;
        onChange(value)
    }

    return (
        <select
            id="brand"
            name="brand"
            value={brand}
            onChange={handleChange}
        >
            <option value="all">All</option>
            {
                Array.isArray(brands) &&
                brands.map(brand => <option key={brand.slug} value={brand.slug}>{brand.description}</option>)
            }
        </select>
    )
}