import { useState, useRef } from "react";
import styled from 'styled-components';

// https://dev.to/chandrapantachhetri/responsive-react-file-upload-component-with-drag-and-drop-4ef8
const DEFAULT_MAX_FILE_SIZE_IN_BYTES = 500000;

const FileUpload = ({
  label,
  // updateFilesCb,
  onChange,
  maxFileSizeInBytes = DEFAULT_MAX_FILE_SIZE_IN_BYTES,
  ...otherProps
}) => {
  const fileInputField = useRef(null);
  // const [files, setFiles] = useState({});

  return (
    // <section>
    //     <label>{label}</label>
    //     <p>Drag and drop your files anywhere or</p>
    //     <button type="button">
    //         <i className="fas fa-file-upload" />
    //         <span> Upload {otherProps.multiple ? "files" : "a file"}</span>
    //     </button>
    //     <input
    //         type="file"
    //         ref={fileInputField}
    //         title=""
    //         value=""
    //         {...otherProps}
    //     />
    // </section>

    <FileUploadContainer>
      <InputLabel>{label}</InputLabel>
      <DragDropText>Drag and drop your files anywhere or</DragDropText>
      <UploadButton><i className="lni lni-upload"></i>Click Here</UploadButton>
      {/* <UploadFileBtn type="button">
                <i className="lni lni-upload"></i>
                <span> Upload {otherProps.multiple ? "files" : "a file"}</span>
            </UploadFileBtn> */}
      <FormField
        type="file"
        ref={fileInputField}
        onChange={onChange}
        title=""
        value=""
        {...otherProps}
      />
    </FileUploadContainer>
  )
}

export default FileUpload;

export const FileUploadContainer = styled.section`
  position: relative;
  margin: 25px 0 15px;
  border: 2px dotted lightgray;
  padding: 35px 20px;
  border-radius: 6px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: white;
  cursor: pointer;
`;


export const FormField = styled.input`
  font-size: 18px;
  display: block;
  width: 100%;
  border: none;
  text-transform: none;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  opacity: 0;

  &:focus {
    outline: none;
  }
`;

export const InputLabel = styled.label`
  top: -25px;
  font-family: Helvetica, sans-serif;
  font-size: 16px;
  color: black;
  left: 0;
  position: absolute;
`;

export const DragDropText = styled.p`
  font-weight: bold;
  letter-spacing: 2.2px;
  margin-top: 0;
  text-align: center;
`;

export const UploadButton = styled.p`
    color: #fff;
    background-color: #EF3B68; 
    padding: 5px 10px; 
    border-radius: 3px; 
    cursor: pointer;

    i {
        margin-right: 1.0rem;
    }
`

// export const UploadFileBtn = styled.span`
//   box-sizing: border-box;
//   appearance: none;
//   background-color: transparent;
//   border: 2px solid #3498db;
//   cursor: pointer;
//   font-size: 1rem;
//   line-height: 1;
//   padding: 1.1em 2.8em;
//   text-align: center;
//   text-transform: uppercase;
//   font-weight: 700;
//   border-radius: 6px;
//   color: #3498db;
//   position: relative;
//   overflow: hidden;
//   z-index: 1;
//   transition: color 250ms ease-in-out;
//   font-family: "Open Sans", sans-serif;
//   width: 45%;
//   display: flex;
//   align-items: center;
//   padding-right: 0;
//   justify-content: center;

//   &:after {
//     content: "";
//     position: absolute;
//     display: block;
//     top: 0;
//     left: 50%;
//     transform: translateX(-50%);
//     width: 0;
//     height: 100%;
//     background: #3498db;
//     z-index: -1;
//     transition: width 250ms ease-in-out;
//   }

//   i {
//     font-size: 22px;
//     margin-right: 5px;
//     position: absolute;
//     top: 0;
//     bottom: 0;
//     left: 0;
//     right: 0;
//     width: 20%;
//     display: flex;
//     flex-direction: column;
//     justify-content: center;
//   }

//   @media only screen and (max-width: 500px) {
//     width: 70%;
//   }

//   @media only screen and (max-width: 350px) {
//     width: 100%;
//   }

//   &:hover {
//     color: #fff;
//     outline: 0;
//     background: transparent;

//     &:after {
//       width: 110%;
//     }
//   }

//   &:focus {
//     outline: 0;
//     background: transparent;
//   }

//   &:disabled {
//     opacity: 0.4;
//     filter: grayscale(100%);
//     pointer-events: none;
//   }
// `;