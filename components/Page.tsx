import styled, { createGlobalStyle } from 'styled-components'
import Footer from './Footer';
import Header from "./Header";
import SideMenu from './SideMenu';

const GlobalStyles = createGlobalStyle`
html {
    --red: #ff0000;
    --black: #ff0000;
    --grey: #3A33A3;
    --light-grey: #eaeaea;
    --main-font-color: #494949;
    --main-color: #EF3B68;
    box-sizing: border-box;

    --bg-color: #fff; 
    --basic-dark-color: #212121; 
    --border-width: 4px; 
    --basic-spinner-dimensions: 125px; 
    --main-spinner-dimensions: var(--basic-spinner-dimensions) - var(--border-width * 2);
    --small-spinner-dimensions: var(--main-spinner-dimensions) * 0.7; 
}
*, *:before, *:after {
    box-sizing:inherit
}
body {
    padding:0;
    margin:0;
    overflow-x: hidden;
}
* {
    padding: 0;
    margin: 0;
    border: 0;
}
`

export default function Page({ children }) {
    return <div >
        <GlobalStyles />
        <Container>
            <SideMenu />
            <div>
                {children}
            </div>
        </Container>
    </div>
}

const Container = styled.div`
    display: grid;
    grid-gap: 0px;
    grid-template-columns: 60px auto;
`