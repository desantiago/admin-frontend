import Link from 'next/link'
import Nav from './Nav'

import styled from 'styled-components';

//style for the bar
// how to use css variables on style components

export default function Header({ children }) {
    return (
        <header >
            <Container>
                {children}
            </Container>
        </header>
    )

    // return (
    //     <header >
    //         <HeaderStyles>
    //             <Link href="/">Always Avantgarde</Link>
    //         </HeaderStyles>
    //         <Nav />
    //     </header>
    // )
}

const Container = styled.div`
    height: 5vh;
    color:#494949;
    border-bottom: 1px solid #eaeaea;
`

const HeaderStyles = styled.h1`
    text-align: center;
    font-family: Josefin Slab, serif;
    font-size: 2.0rem;
    padding: 0.3rem;
    width: 100vw;
    height: 5vh;
    background-color:#eaeaea;
    color:#494949;

    a {
        color: var(--main-font-color);
        text-decoration: none;
    }
`
