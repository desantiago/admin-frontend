import styled, { css } from 'styled-components';

const Button = styled.button` 
width: auto;
border: 0;
font-size: 1.0rem;
font-weight: 600;
padding: 0.25rem 1.2rem;
margin: 0.25rem;

${props => props.primary && css`
background: var(--main-color);
color: white;
`}

${props => props.secondary && css`
background: var(--light-grey);
color: var(--main-font-color);
`}

`

export default Button;