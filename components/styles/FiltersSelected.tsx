import styled from 'styled-components';

export const FiltersSelected = styled.div`
font-family: Helvetica, Arial, sans-serif;
font-size: 0.8rem;
`

export const FilterOption = styled.a`
color: inherit;
text-decoration: none;
margin-left: 2.0rem;
i {
  margin-left: 0.25rem;
  font-weight: bold;
}
`
