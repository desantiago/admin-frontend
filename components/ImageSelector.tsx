import styled from 'styled-components';
import { File } from '../lib/useForm';

import { Image } from 'cloudinary-react';
import { useCombobox } from 'downshift';
import { cloudName } from '../config';

interface ImageSelectorProps {
    files: File[],
    image: File,
    setImage: Function
    title: string
}

export default function ImageSelector({ files, image, setImage, title }: ImageSelectorProps) {
    if (!image) return <div></div>

    const {
        isOpen,
        getMenuProps,
        getInputProps,
        getComboboxProps,
        getItemProps,
        getToggleButtonProps,
        highlightedIndex
    } = useCombobox({

        items: files,
        onInputValueChange({ inputValue }) { },
        onSelectedItemChange({ selectedItem }) {
            // console.log(selectedItem);
            setImage(selectedItem.id);
            // router.push({
            //     pathname: `/product/${selectedItem.key}`,
            // });
        },
        itemToString: (item: any) => item?.filename || '',
    })

    return (
        <ImageStyles>
            <h3>{title}</h3>
            <div {...getComboboxProps()}>
                <input type="hidden" {...getInputProps()} />
                <button
                    type="button"
                    {...getToggleButtonProps()}
                    aria-label="toggle menu"
                >
                    <Image cloudName={cloudName} publicId={image.public_id} width="80" crop="scale" />
                </button>
            </div>
            <DropDown {...getMenuProps()}>
                {
                    isOpen && files.map((file: File, index) => {
                        return (
                            <DropDownItem
                                key={file.id}
                                {...getItemProps({ file, index })}
                                highlighted={index === highlightedIndex}
                            >
                                <Image cloudName={cloudName} publicId={file.public_id} width="100" crop="scale" />
                            </DropDownItem>
                        )
                    })
                }
                {isOpen && !files.length && (
                    <DropDownItem>Sorry, No items found </DropDownItem>
                )}
            </DropDown>
        </ImageStyles>
    )
}

const DropDown = styled.div`
    background-color: white;
    position: absolute;
    width: 300px;
    z-index: 2;
    
    display: grid;
    grid-gap: 0px;
    grid-template-columns: repeat(auto-fill, minmax(70px, 1fr));

`;

const DropDownItem = styled.div`
    background-color: white;
    padding: 0.2rem;
    img {
        margin: 0.2rem;
        width: 70px;
    }
`;

const ImageStyles = styled.div`
    position: relative;
    margin: 1.0rem;

    h3 {
        font-family: 'Helvetica';
        font-size: 0.9rem;
    }

    button {
        background-color: white;
    }
`;
