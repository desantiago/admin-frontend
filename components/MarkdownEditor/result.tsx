import styled from "styled-components";
import ReactMarkdown from "react-markdown";
import { useMarkdownContext } from "./context";

export default function Result(props) {
  const { markdownText } = useMarkdownContext();

  return (
    <Container>
      {/* <Title>Converted Text</Title> */}
      <Title>Preview</Title>
      <ResultArea>
        <ReactMarkdown source={markdownText} />
      </ResultArea>
    </Container>
  );
}

const Container = styled.div`
  width: 100%;
  height: 100%;

  font-family: "Lato", sans-serif;
`;

// const Title = styled.div`
//   font-size: 22px;
//   font-weight: 600;
//   margin-bottom: 1em;
//   padding: 8px 0;
//   border-bottom: 1px solid rgba(15, 15, 15, 0.3);
// `;

const Title = styled.div`
  font-size: 12px;
  font-weight: 600;
  padding: 6px 0;
  border-bottom: 1px solid rgba(15, 15, 15, 0.3);
`;

const ResultArea = styled.div`
  width: 100%;
  height: 100%;
  border: none;
  font-size: 14px;
  padding-left: 1.0rem;
  padding-right: 1.0rem;
  padding-top: 0.25rem;
`;
