import styled from 'styled-components';
import OrderDetails from './OrderDetails';
import OrderList from './OrderList';
import { OrdersStateProvider } from '../lib/ordersContext';

export default function OrdersAdmin() {
    return (
        <OrdersStateProvider>
            <MainWraper>
                <SideBar>
                    <OrderList></OrderList>
                </SideBar>
                <Content>
                    <OrderDetails></OrderDetails>
                </Content>
            </MainWraper>
        </OrdersStateProvider>
    )
}

const MainWraper = styled.div`
    display:block;

    @media only screen and (min-width: 800px)   {
        height: 95vh;
        display: grid;
        grid-gap: 0px;
        grid-template-columns: auto auto;
        grid-template-areas:
        "sidebar content"
    }
`

const SideBar = styled.aside`
    display: block;
    grid-area: sidebar;
    height: 95vh;
    overflow: auto;
`;

const Content = styled.main`
    grid-area: content;
    height: 95vh;
    overflow: auto;
`