
import { gql, useMutation } from '@apollo/client';
import styled from 'styled-components';
import Drawer from "./Drawer";
import useFormSimple from "../lib/useFormSimple";
import { useToasts } from 'react-toast-notifications';

import {
    ALL_COLORS,
} from '../lib/queryProducts';

const CREATE_COLOR_MUTATION = gql`
    mutation CREATE_COLOR_MUTATION(
        $name: String!
        $hex: String
    ) {
        createColor(name: $name, hex: $hex) {
            id
        }
    }
`

export default function CreateColor({ open, onClose }) {
    const { addToast } = useToasts();

    const {
        inputs,
        handleChange,
        clearForm
    } = useFormSimple({ colorName: '', hex: '' });

    // const [createColor, { loading, error, data }] = useMutation(CREATE_COLOR_MUTATION, {
    //     variables: inputs,
    //     refetchQueries: [{
    //         query: ALL_COLORS, variables: {}
    //     }]
    // });
    const [createColor, { loading, error, data }] = useMutation(CREATE_COLOR_MUTATION);

    const saveColor = async () => {
        const res = await createColor({
            variables: {
                name: inputs.colorName,
                hex: inputs.hex
            },
            refetchQueries: [{
                query: ALL_COLORS, variables: {}
            }]
        });

        if (res.data.createColor.id) {
            clearForm();
            addToast("Color was inserted", {
                appearance: 'success',
                autoDismiss: true,
            });
        }
    }

    // <fieldset disabled={loading} aria-busy={loading}>
    return (
        <Drawer title="Colors" open={open} onClose={onClose}>
            <Form onSubmit={(e) => {
                e.preventDefault();
                saveColor();
            }}>
                <fieldset disabled={loading} aria-busy={loading}>
                    <label htmlFor="name">
                        Color Name
                        <input
                            type="text"
                            value={inputs.colorName}
                            id="colorName" name="colorName"
                            placeholder="Name"
                            onChange={handleChange} />
                    </label>

                    <label htmlFor="hex">
                        Hex
                        <input
                            type="text"
                            value={inputs.hex}
                            id="hex" name="hex"
                            placeholder="Hex"
                            onChange={handleChange} />
                    </label>

                    <button type="submit">Add Color</button>
                </fieldset>
            </Form>
        </Drawer>
    )
}

const Form = styled.form`
    min-width: 350px;
    max-width: 550px;
    font-size: 1.15rem;
    line-height: 1.5;
    font-weight: 500;
    font-family: Helvetica;
    label {
        display: block;
        margin-bottom: 1rem;
    }
    input,
    textarea,
    select {
        width: 100%;
        padding: 0.5rem;
        font-size: 1rem;
        border: 1px solid #989898;
        &:focus {
            outline: 0;
            border-color: var(--main-color);
        }
    }
    button,
    input[type='submit'] {
        width: auto;
        background: var(--main-color);
        color: white;
        border: 0;
        font-size: 1.5rem;
        font-weight: 600;
        padding: 0.25rem 1.2rem;
    }
    fieldset {
        border: 0;
        padding: 0;
        
        &[disabled] {
            opacity: 0.5;
        }
    }
`
