import { useState, useContext } from 'react';
import styled from 'styled-components';
import { Image } from 'cloudinary-react';

import ProductContext from '../lib/productContext';
import { cloudName } from '../config';

export default function ProductList() {
    const {
        product,
        products,
        selectProduct,
        selectedProduct,
        categories,
        brands,
        setSelectedBrand,
        setSelectedSubCategory,
        pristine,
    }: any = useContext(ProductContext);

    const [subCategory, setSubCategory] = useState('');
    const [brand, setBrand] = useState('all');

    const handleChange = (e) => {
        let { value, name, type } = e.target;

        if (name === 'subCategory') {
            setSubCategory(value);
            setSelectedSubCategory([value]);
        }
        if (name === 'brand') {
            setBrand(value);
            setSelectedBrand(value === 'all' ? undefined : [value]);
        }
    }

    const onSelectProduct = (id: string): void => {
        if (!pristine) {
            if (confirm("You have made changes, are you want load new data and lost your changes?")) {
                selectProduct(id);
            }
        }
        else {
            selectProduct(id);
        }
    }

    // console.log(products);

    return (
        <div>
            <Filters>
                <select
                    id="subCategory"
                    name="subCategory"
                    value={subCategory}
                    onChange={handleChange}>
                    {
                        Array.isArray(categories) && categories.map(category => {
                            return (
                                <optgroup label={category.description} key={category.slug}>
                                    {
                                        category.subcategories.map(subCategory => <option key={subCategory.id} value={subCategory.slug}>{subCategory.description}</option>)
                                    }
                                </optgroup>
                            )
                        })
                    }
                </select>

                <select
                    id="brand"
                    name="brand"
                    value={brand}
                    onChange={handleChange}
                >
                    <option value="all">All</option>
                    {
                        Array.isArray(brands) && brands.map(brand => <option key={brand.slug} value={brand.slug}>{brand.description}</option>)
                    }
                </select>
            </Filters>

            {
                Array.isArray(products) && products.map(pr => {
                    const { thumbnail, brand, name, id } = pr;
                    return (
                        <Product
                            key={id}
                            onClick={() => { onSelectProduct(id) }}
                            // className={id === product && product.id ? 'selected' : ''}
                            // className={product && product.id && id === product.id ? 'selected' : ''}
                            className={id === selectedProduct ? 'selected' : ''}
                        >
                            {/* <Img src={"http://127.0.0.1:8080/" + images[0].images[0].filename} /> */}
                            <Image cloudName={cloudName} publicId={thumbnail.image.public_id} width="60" crop="scale" />

                            <ProductData >
                                <Brand>
                                    {brand.description}
                                </Brand>
                                <Name>
                                    {name}
                                </Name>
                            </ProductData>
                        </Product>
                    )
                })
            }
        </div>
    )
}

const Filters = styled.div`
    display: grid;
    grid-template-columns: auto auto;
    margin: 0.25rem;
    padding: 0.25rem;
    border: 1px solid #eaeaea;    
`

const Product = styled.div`
    display: grid;
    grid-template-columns: 60px auto;
    grid-auto-rows: 1fr;
    font-family: 'Mada', sans-serif;
    font-size: 1.0rem;
    align-items: center;
    border-bottom: 1px solid #eaeaea;
    cursor:pointer;

    &.selected {
        background-color:#eaeaea;
    }
`
const Brand = styled.p`
    padding: 1px 3px 1px 3px;
    color: #494949;
    font-weight: bold;
    font-size: 0.8rem;
`
const Name = styled.p`
    padding: 1px 3px 1px 3px;
    color: #494949;
`

const ProductData = styled.div`
`
