import styled from 'styled-components';
import { useOrders } from '../lib/ordersContext';
import moment from 'moment'
import formatMoney from '../lib/formatMoney';

export default function OrderDetails() {

    const { order } = useOrders();
    console.log(order);
    return (
        <OrderStyles>
            <p>
                <span>Order Id:</span>
                <span>{order && order.key}</span>
            </p>
            <p>
                <span>Order No.:</span>
                <span>{order && order.id}</span>
            </p>
            <p>
                <span>Total:</span>
                <span>{order && formatMoney(order.total)}</span>
            </p>
            <p>
                <span>Date:</span>
                <span>{order && moment(order.date, 'x').format('MM/DD/YYYY')}</span>
            </p>
            <p>
                <span>Customer:</span>
                <span>{order && order.user && order.user.name}</span>
            </p>
            <CartItemContainer>
                {
                    order && Array.isArray(order.orderItems) && order.orderItems.map(item => {
                        return (
                            <CartItem>
                                <img src={`http://127.0.0.1:8080/${item.product.images[0].images[0].filename}`}></img>
                                <div>
                                    <CartBrand>{item.product.brand.description}</CartBrand>
                                    <p>{item.product.name}</p>
                                </div>
                                <p>{item.size}</p>
                                <p>{item.color.name}</p>
                                <p>{item.quantity}</p>
                                <Total>{formatMoney(item.price)}</Total>
                                <Total>{formatMoney(item.total)}</Total>
                            </CartItem>
                        )
                    })
                }
                <CartItem>
                    <img src={'http://127.0.0.1:8080/9XX03186703_1.jpeg'}></img>
                    <div>
                        <CartBrand>Manolo Blahnik</CartBrand>
                        <p>Hangisi Crystal Pumps</p>
                    </div>
                    <p>11</p>
                    <p>HotPink</p>
                    <p>1</p>
                    <Total>{995}</Total>
                    <Total>{995}</Total>
                </CartItem>
                <CartItem>
                    <img src={'http://127.0.0.1:8080/MALVINABOOTBLK_1.jpeg'}></img>
                    <div>
                        <CartBrand>Manolo Blahnik</CartBrand>
                        <p>Alex Pantent Leather Booties</p>
                    </div>
                    <p>11</p>
                    <p>HotPink</p>
                    <p>1</p>
                    <Total>{995}</Total>
                    <Total>{995}</Total>
                </CartItem>
            </CartItemContainer>
        </OrderStyles>
    )
}

const OrderStyles = styled.div`
    font-family: 'Mada', sans-serif;

    margin: 1.0rem;
    border: 1px solid #eaeaea;
    border-radius: 3px;

    & > p {
        display: grid;
        grid-template-columns: 1fr 5fr;
        margin: 0;
        border-bottom: 1px solid var(--offWhite);
        span {
          padding: 0.5rem;
          &:first-child {
            font-weight: 900;
            text-align: right;
          }
        }
    }
`

const CartItemContainer = styled.ul`
    margin-left: 2.0rem;
    margin-right: 2.0rem;
`

const CartName = styled.span`
    font-size: 1.5rem;
`
const CartBrand = styled.p`
    font-weight: bold;
`
const CartProductName = styled.p`
`
const CartProductDetails = styled.p`
`

const Total = styled.p`
    font-weight: bold;
`
const CartItem = styled.li`
    font-family: 'Mada', sans-serif;
    border-bottom: 1px solid #eaeaea;
    padding: 1rem 0;

    display: grid;
    grid-template-columns: 60px auto 80px 80px 80px 80px 80px;

    &:last-child {
        border-bottom: none;
    }
    
    img {
        width: 50px;
        height: auto;
        margin-right: 1rem;
    }
    h3,
    div {
        
    }
`;
