import CreateProduct from "../components/CreateProduct";
import { gql } from "@apollo/client";
import { GetStaticProps } from 'next'
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { ProductProvider } from '../lib/productContext';
import { ToastProvider } from 'react-toast-notifications';

const ALL_QUERIES = gql`
    query ALL_QUERIES {
        categories {
            key
            description
            subcategories {
                key
                description
            }
        }
        colors {
            key
            name
            hex
        }
        brands {
            description,
            key
        }          
    }
`

import { DefaultToast } from 'react-toast-notifications';
export const MyCustomToast = ({ children, ...props }) => (
    <DefaultToast {...props}>
        <p style={{ fontFamily: "sans-serif" }}>{children}</p>
    </DefaultToast>
);

function AddProduct({ categories, colors, brands }) {
    return (
        <ProductProvider>
            <ToastProvider
                components={{ Toast: MyCustomToast }}
                placement="top-center"
            >
                <CreateProduct />
            </ToastProvider>
        </ProductProvider>
    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log("context: ", context);

    const client2 = new ApolloClient({
        uri: 'http://localhost:3000/graphql',
        cache: new InMemoryCache()
    });

    const { data } = await client2.query({
        query: ALL_QUERIES
    });

    return {
        props: {
            categories: data.categories,
            colors: data.colors,
            brands: data.brands,
            // products: products.products
        }
    }
}

export default AddProduct;