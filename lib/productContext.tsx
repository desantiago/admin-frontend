import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { gql, useLazyQuery, useQuery } from '@apollo/client';

import {
    PRODUCT_LIST_QUERY,
    ALL_BRANDS,
    ALL_CATEGORIES,
    ALL_COLORS,
    PRODUCT_QUERY
} from './queryProducts';

const ProductContext = React.createContext({});

export const ProductProvider = props => {
    const [pristine, setPristine] = useState(true);
    const [selectedProduct, setSelectedProduct] = useState('');
    const [selectedSubCategory, setSelectedSubCategory] = useState(['pumps']);
    const [selectedBrand, setSelectedBrand] = useState(undefined);

    // console.log('selectedSubCategory', selectedSubCategory);
    const { loading, data } = useQuery(PRODUCT_LIST_QUERY, {
        variables: {
            category: [],
            subCategory: selectedSubCategory,
            brands: selectedBrand
        },
        fetchPolicy: "network-only"
    });
    const products: any[] = loading || data.products;

    const { loading: loadingBrands, data: dataBrands } = useQuery(ALL_BRANDS);
    const brands: any[] = loadingBrands || dataBrands.brands;

    const { loading: loadingCat, data: dataCat } = useQuery(ALL_CATEGORIES);
    const categories: any[] = loadingCat || dataCat.categories;

    const { loading: loadingColors, data: dataColors } = useQuery(ALL_COLORS);
    const colors: any[] = loadingColors || dataColors.colors;

    const [
        getProduct,
        { loading: loadingProduct, data: dataProduct }
    ] = useLazyQuery(PRODUCT_QUERY, {
        fetchPolicy: "network-only"
    });
    const product: any[] = loadingProduct || dataProduct && dataProduct.product;

    const selectProduct = (id: string) => {
        getProduct({
            variables: {
                key: id
            }
        });
        setSelectedProduct(id);
    }

    const values = {
        products,
        brands,
        categories,
        colors,
        selectProduct,
        product,
        selectedProduct,
        selectedSubCategory,
        selectedBrand,
        setSelectedProduct,
        setSelectedSubCategory,
        setSelectedBrand,
        pristine,
        setPristine
    }

    return (
        <ProductContext.Provider value={values}>{props.children}</ProductContext.Provider>
    );
};
export const ProductConsumer = ProductContext.Consumer;
export default ProductContext;

ProductProvider.propTypes = {
    children: PropTypes.node.isRequired,
};