import { useLazyQuery, useQuery } from '@apollo/client';
import { useState, useContext } from 'react';
import lodash from 'lodash';
import { client } from './withData';
import ProductContext from '../lib/productContext';

import { CHECK_SLUG_QUERY } from './queryProducts';

export interface Color {
    id: string
    name: string
    hex: string
}

export interface File {
    id: string
    public_id: string
    filename?: string
    mimetype?: string
    encoding?: string
}

export interface Image {
    file: string
    order: number
}

export interface Images {
    color: string
    images: Image[]
}

export interface Price {
    size: string,
    price: number
}

export interface Prices {
    color: string
    prices: Price[]
}

export interface Sizes {
    color: string
    sizes: string[]
}

export interface ImageColor {
    color: string;
    image: string;
}

export interface ValidationMessages {
    error: boolean;
    message: string;
}

export interface FormFields {
    name: string,
    externalId: string,
    slug: string,
    price: number,
    description: string,
    sizeAndFit: string,
    details: string,
    brand: string,
    category: string,
    subCategory: string,
    startSize: string,
    endSize: string,
    colors: string[],
    images: Images[],
    sizes: Sizes[],
    prices: Prices[],
    files: File[],
    imagesStack: any[]
    thumbnail: ImageColor;
    mainImage: ImageColor;
    thumbnailImg: File,
    mainImgImg: File,
    relatedProducts: any,
    relatedProductsData: any,
    slugExists: boolean;
}

export function useForm(initial: FormFields = {
    name: '',
    externalId: '',
    slug: '',
    price: 0,
    description: '',
    sizeAndFit: '',
    details: '',
    colors: [],
    images: [],
    sizes: [],
    prices: [],
    brand: '',
    category: '',
    subCategory: '',
    startSize: '',
    endSize: '',
    files: [],
    imagesStack: [],
    thumbnail: null,
    mainImage: null,
    thumbnailImg: null,
    mainImgImg: null,
    relatedProductsData: [],
    relatedProducts: [],
    slugExists: false
}, brands) {
    const [inputs, setInputs] = useState(initial);
    const [colorSelected, setColorSelected] = useState('');
    // const [pristine, setPristine] = useState(true);

    const {
        pristine,
        setPristine,
    }: any = useContext(ProductContext);

    const [search] = useLazyQuery(CHECK_SLUG_QUERY, {
        fetchPolicy: 'no-cache',
        onCompleted: (data) => {
            console.log("slug data", data);
            //set
            setInputs({
                ...inputs,
                slugExists: data.productByIds ? true : false
            })
        }
    });

    const { refetch } = useQuery(CHECK_SLUG_QUERY, {
        fetchPolicy: 'no-cache'
    });

    //const items = data?.searchTerms || [];
    // console.log(items);
    const findSlugButChill = lodash.debounce(async (id) => {
        // console.log('findSlugButChill', id);
        search({
            variables: {
                id: id
            },
        });
    }, 350);

    function handleChange(e): void {
        let { value, name, type } = e.target;
        if (type === 'number') {
            value = Number(value);
        }
        if (type === 'file') {
            [value] = e.target.files;
        }

        if (name === 'slug') {
            findSlugButChill(value);
        }
        if (name === 'price') {
            updatePrice(Number(value));
        }
        else if (name === 'startSize') {
            updateSizes(value, inputs.endSize);
        }
        else if (name === 'endSize') {
            updateSizes(inputs.startSize, value);
        }
        else {
            setInputs({
                ...inputs,
                [e.target.name]: e.target.value
            });
        }
        setPristine(false);
    }

    function getSizesAndPrices() {
        const { startSize, endSize, price } = inputs;
        const start = Number(startSize);
        const end = Number(endSize);

        let newSizes = [];
        let newPrices = []
        if (start && endSize) {
            for (let i = start; i <= end; i++) {
                newSizes.push(i.toString());
                newPrices.push({
                    size: i.toString(),
                    price: Number(price)
                });
            }
        }
        return { newSizes, newPrices };
    }

    function addColorRecord(id: string) {
        const { images, prices, sizes } = inputs;
        const { newSizes, newPrices } = getSizesAndPrices();
        images.push({
            color: id,
            images: []
        });
        prices.push({
            color: id,
            prices: [...newPrices]
        });
        sizes.push({
            color: id,
            sizes: [...newSizes]
        });

        return {
            images,
            prices,
            sizes
        }
    }

    function removeColorRecord(colorId: string) {
        const { images, prices, sizes } = inputs;
        let index = -1;

        index = images.findIndex(image => image.color === colorId);
        images.splice(index, 1);

        index = prices.findIndex(price => price.color === colorId);
        prices.splice(index, 1);

        index = sizes.findIndex(size => size.color === colorId);
        sizes.splice(index, 1);

        return {
            images,
            prices,
            sizes
        }
    }

    function addColor(colorId: string): any {
        const { colors } = inputs;
        const { images, prices, sizes } = addColorRecord(colorId);
        colors.push(colorId);

        setPristine(false);
        return {
            colors,
            images,
            prices,
            sizes
        }
    }

    function removeColor(colorId: string, index: number): any {
        const { colors } = inputs;
        const { images, prices, sizes } = removeColorRecord(colorId);
        colors.splice(index, 1);

        setPristine(false);
        return {
            colors,
            images,
            prices,
            sizes
        }
    }

    function setColor(id: string): void {
        // const { colors } = inputs;
        var index = inputs.colors.indexOf(id);
        const { colors, images, prices, sizes } = index !== -1 ? removeColor(id, index) : addColor(id)

        //If a color is being added, set the color selected
        if (index === -1) {
            setColorSelected(id);
        }
        else {
            //If the color is being removed
            if (colors.length) {
                //Select the first color 
                setColorSelected(colors[0]);
            }
            else {
                //Set the color selected to empty
                setColorSelected('');
            }
        }

        setInputs({
            ...inputs,
            colors,
            images,
            prices,
            sizes
        });
    }

    function updatePrice(price: number): void {
        let { prices } = inputs;

        // let pricesToUpdate = [...prices];

        // pricesToUpdate.forEach((_, index) => {
        //     pricesToUpdate[index].prices.forEach((_, indexp) => {
        //         console.log('', pricesToUpdate[index].prices[indexp].price);
        //         pricesToUpdate[index].prices[indexp].price = Number(price);
        //     });
        // });
        // console.log(prices);

        prices = JSON.parse(JSON.stringify(prices));
        prices.forEach((pricesRoot, index) => {
            pricesRoot.prices.forEach((prices2, indexp) => {
                console.log(prices2.price);
                prices2.price = Number(price);
            });
        });

        setInputs({
            ...inputs,
            prices,
            price
        });
    }

    function updateSizes(startSize: string, endSize: string): void {
        const { prices, price, sizes } = inputs;
        const start = Number(startSize);
        const end = Number(endSize);
        // console.log("updateSizes", start, end);
        if (start && end) {
            let newSizes = [];
            for (let i = start; i <= end; i++) {
                newSizes.push(i.toString());
            }

            sizes.forEach(sizeColor => {
                sizeColor.sizes = [...newSizes];
            });

            prices.forEach(priceColor => {
                // check for new possible sizes to add
                let availableSizes = priceColor.prices.map(price => price.size);
                newSizes.forEach(size => {
                    if (!availableSizes.includes(size)) {
                        priceColor.prices.push({
                            size: size,
                            price: Number(price)
                        });
                        availableSizes.push(size);
                    }
                });
                // check for possible sizes to remove
                availableSizes.forEach(size => {
                    if (!newSizes.includes(size)) {
                        const index = priceColor.prices.findIndex(price => price.size === size)
                        priceColor.prices.splice(index, 1);
                    }
                })
            })

            setInputs({
                ...inputs,
                prices,
                sizes,
                startSize,
                endSize
            });
        }
        else {
            setInputs({
                ...inputs,
                startSize,
                endSize
            });
        }
    }

    function changePrice(color: string, size: string, value: number): void {
        const { prices } = inputs;
        const priceColor = prices.find(price => price.color === color);
        const indexPrice = priceColor.prices.findIndex(price => price.size === size);
        priceColor.prices[indexPrice].price = value;

        setInputs({
            ...inputs,
            prices
        });
    }

    function removeSizeFromList(color: string, size: string): void {
        const { prices, sizes } = inputs;
        const priceColor = prices.find(price => price.color === color);
        const indexPrice = priceColor.prices.findIndex(price => price.size === size);
        priceColor.prices.splice(indexPrice, 1);

        const sizeColor = sizes.find(size => size.color === color);
        const indexSize = sizeColor.sizes.indexOf(size);
        sizeColor.sizes.splice(indexSize, 1);

        setInputs({
            ...inputs,
            prices,
            sizes
        });
    }

    function removeImageFromList(color: string, imageKey: string): void {
        const { images, thumbnail, thumbnailImg, mainImage, mainImgImg } = inputs;
        const sizeColor = images.find(image => image.color === color);
        // const indexImage = sizeColor.images.findIndex(image => image.key === imageKey);
        // const indexImage = sizeColor.images.indexOf(imageKey);
        const indexImage = sizeColor.images.findIndex(image => image.file === imageKey);

        sizeColor.images.splice(indexImage, 1);

        setInputs({
            ...inputs,
            images: [...images],
            imagesStack: [...images.map(image => {
                return {
                    color: image.color,
                    images: image.images.map(file => file.file)
                }
            })],
            thumbnail: thumbnail.image === imageKey ? null : thumbnail,
            thumbnailImg: thumbnail.image === imageKey ? null : thumbnailImg,
            mainImage: mainImage.image === imageKey ? null : mainImage,
            mainImgImg: mainImage.image === imageKey ? null : mainImgImg,
        });
    }

    function addImages(color: string, newImages: File[]): void {
        const { images } = inputs;
        const indexColor = images.findIndex(image => image.color === color);

        const fileIds: Image[] = newImages.map((image: File) => {
            return {
                file: image.id,
                order: 0
            }
        });

        images[indexColor].images = [...images[indexColor].images, ...fileIds];

        setInputs({
            ...inputs,
            images
        });
    }

    function swapImages(color: string, sourceId: string, destinyId: string): void {
        const { imagesStack, images } = inputs;
        const indexColor = imagesStack.findIndex(image => image.color === color);
        const imagesColor = imagesStack[indexColor].images;

        const sourceIndex: number = imagesColor.findIndex(image => image === sourceId);
        const destinyIndex: number = imagesColor.findIndex(image => image === destinyId);

        // remove image from original position
        const image = imagesColor.splice(sourceIndex, 1);
        // insert image on the position where it was dropped 
        imagesColor.splice(destinyIndex, 0, image[0]);

        const imagesOrder = images[indexColor].images;

        imagesColor.forEach((img, index) => {
            imagesOrder.find(imageOrder => imageOrder.file === img).order = index;
        });

        imagesStack[indexColor].images = [...imagesColor]
        images[indexColor].images = [...imagesOrder]
        // console.log(images);

        setInputs({
            ...inputs,
            imagesStack
        });
    }

    function resetForm() {
        setInputs(initial);
    }

    function clearForm() {
        //const blankState:FormFields = Object.fromEntries(Object.entries(inputs).map(([key, value]) => [key, '']))
        let blankState: FormFields = { ...inputs };
        Object.keys(blankState).forEach(key => {
            blankState[key] = '';
        })
        setInputs(blankState);
    }

    function setFiles(color: string, filesToAdd: File[]): void {
        const {
            images,
            thumbnail,
            mainImage,
            thumbnailImg,
            mainImgImg,
            files
        } = inputs;

        const indexColor = images.findIndex(image => image.color === color);

        // const fileIds = files.map(image => image.id);
        const fileIds: Image[] = filesToAdd.map((image: File, index: number) => {
            return {
                file: image.id,
                order: index + files.length
            }
        });

        images[indexColor].images = [...images[indexColor].images, ...fileIds];

        setInputs({
            ...inputs,
            files: files.concat(filesToAdd),
            images,
            imagesStack: [...images.map(image => {
                return {
                    color: image.color,
                    images: image.images.map(file => file.file)
                }
            })],
            thumbnail: thumbnail ? thumbnail : { color, image: fileIds[0].file },
            mainImage: mainImage ? mainImage : { color, image: fileIds[0].file },
            thumbnailImg: thumbnail ? thumbnailImg : filesToAdd[0],
            mainImgImg: thumbnail ? mainImgImg : filesToAdd[0],
        });
    }

    function loadProduct(product): void {
        if (!product.name) return;

        const images = product.images.map(image => {
            return image.images.map(img => img.file)
        }).flat();
        // console.log("array images", images);

        const newInputs = {
            ...product,
            colors: product.colors.map(color => color.id),
            prices: [...product.prices.map(price => {
                return {
                    color: price.color.id,
                    prices: price.prices
                }
            })],
            images: [...product.images.map(image => {
                return {
                    color: image.color.id,
                    images: image.images.map(img => {
                        return {
                            file: img.file.id,
                            order: img.order
                        }
                    })
                }
            })],
            imagesStack: [...product.images.map(image => {
                return {
                    color: image.color.id,
                    images: image.images.map(img => {
                        return {
                            file: img.file.id,
                            order: img.order
                        }
                    }).sort((a, b) => a.order - b.order).map(img => img.file)
                }
            })],
            sizes: [...product.sizes.map(size => {
                return {
                    color: size.color.id,
                    sizes: size.sizes
                }
            })],
            category: product.category.id,
            subCategory: product.subCategory.id,
            brand: product.brand.id,
            thumbnail: {
                color: product.thumbnail.color.id,
                image: product.thumbnail.image.id
            },
            mainImage: {
                color: product.mainImage.color.id,
                image: product.mainImage.image.id
            },
            thumbnailImg: images.find(file => { return file.id === product.thumbnail.image.id }),
            mainImgImg: images.find(file => { return file.id === product.mainImage.image.id }),
            relatedProductsData: product.relatedProducts.map(related => {
                return {
                    id: related.product.id,
                    name: related.product.name,
                    brand: related.product.brand.description,
                    public_id: related.product.thumbnail.image.public_id,
                    order: related.order
                }
            }).sort((a, b) => a.order - b.order),
            relatedProducts: product.relatedProducts.map(related => {
                return {
                    product: related.product.id,
                    order: related.order
                }
            }).sort((a, b) => a.order - b.order),
            files: images
        }

        setInputs(newInputs);
        setPristine(true);
    }

    function initialValues(values): void {
        setPristine(true);
        setInputs(values);
    }

    function setSlug(slug: string): void {
        setInputs({
            ...inputs,
            slug
        });
    }

    function setThumbnail(id: string): void {
        // console.log('setThumbnail', id);
        const thumbnailImg: File = inputs.files.find(file => {
            return file.id === id
        });

        setInputs({
            ...inputs,
            thumbnail: {
                ...inputs.thumbnail,
                image: id
            },
            thumbnailImg
        });
    }

    function setMainImg(id: string): void {
        // console.log('setMainImg', id);
        const mainImgImg: File = inputs.files.find(file => {
            return file.id === id
        });

        setInputs({
            ...inputs,
            mainImage: {
                ...inputs.mainImage,
                image: id
            },
            mainImgImg
        });
    }

    function getRelatedProducts(relatedProductsData: any): [] {
        return relatedProductsData.map((related, index) => {
            return {
                product: related.id,
                order: index
            }
        })
    }

    function setRelatedProducts(relatedProductsData: any): void {
        setInputs({
            ...inputs,
            relatedProductsData,
            relatedProducts: getRelatedProducts(relatedProductsData)
        })
    }

    function swapRelatedProducts(sourceId: string, destinyId: string): void {
        const { relatedProductsData } = inputs;

        const sourceIndex: number = relatedProductsData.findIndex(related => related.id === sourceId);
        const destinyIndex: number = relatedProductsData.findIndex(related => related.id === destinyId);

        // remove product from original position
        const product = relatedProductsData.splice(sourceIndex, 1);
        // insert product on the position where it was dropped 
        relatedProductsData.splice(destinyIndex, 0, product[0]);

        setInputs({
            ...inputs,
            relatedProductsData: relatedProductsData.map((related, index) => {
                return {
                    ...related,
                    order: index
                }
            }),
            relatedProducts: getRelatedProducts(relatedProductsData)
        });
    }

    async function getSlug(slug: string) {
        const { data } = await client.query({
            query: CHECK_SLUG_QUERY,
            variables: {
                id: slug
            }
        });

        return data;
    }

    async function validate(): Promise<ValidationMessages> {
        const { name, slug, price, colors, sizes, prices, images, thumbnail, mainImage } = inputs;

        if (!name || name.length === 0) return { error: true, message: 'The product must have a name' };
        if (!slug || slug.length === 0) return { error: true, message: 'The product must have a slug' };

        const res = await getSlug(slug);
        // console.log("response from slug", res);
        if (res.productByIds) return { error: true, message: 'The slug already exists' }

        if (!colors || colors.length === 0) return { error: true, message: 'The product must have at least one color' };
        if (!sizes || sizes.length === 0) return { error: true, message: 'The product must have at least one size' };
        const allSizes: boolean = sizes.every(sizeColor => sizeColor.sizes.length ? true : false);
        if (!allSizes) return { error: true, message: 'The product must have at least one size' };
        if (!price || price <= 0) return { error: true, message: 'The product must have a price greater than 0' };

        const allprices = prices.map((priceColor) => {
            return priceColor.prices.filter(price => price.price <= 0);
        }).flat();

        if (allprices.length) return { error: true, message: 'The product has a price that is 0' };

        const allimages: boolean = images.every(imageColor => imageColor.images.length ? true : false);
        if (!allimages) return { error: true, message: 'The product doesn\'t have all the required images' }

        if (!thumbnail || !thumbnail.image || !thumbnail.image.length) {
            return { error: true, message: 'The product must have a thumbnail' };
        }

        if (!mainImage || !mainImage.image || !mainImage.image.length) {
            return { error: true, message: 'The product must have a main image' };
        }

        return {
            error: false,
            message: ''
        };
    }


    return {
        inputs,
        handleChange,
        clearForm,
        setColor,
        updatePrice,
        updateSizes,
        removeImageFromList,
        removeSizeFromList,
        changePrice,
        addImages,
        loadProduct,
        initialValues,
        setFiles,
        setSlug,
        setThumbnail,
        setMainImg,
        setRelatedProducts,
        swapImages,
        swapRelatedProducts,
        colorSelected,
        setColorSelected,
        validate,
        pristine
    }
}